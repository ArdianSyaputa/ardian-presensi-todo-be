package com.example.PresensiToDoList.dto;

import com.example.PresensiToDoList.enumated.PrsensiEnum;

public class PresensiDto {

    private Long usersId;

    private PrsensiEnum presensi;

    public PrsensiEnum getPresensi() {
        return presensi;
    }

    public void setPresensi(PrsensiEnum presensi) {
        this.presensi = presensi;
    }

    public Long getUsersId() {
        return usersId;
    }

    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }
}