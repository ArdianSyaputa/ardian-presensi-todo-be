package com.example.PresensiToDoList.modal;

import com.example.PresensiToDoList.enumated.PrsensiEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "presensi")
public class Presensi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(pattern = "HH:mm")
    @Column(name = "masuk")
    private Date masuk;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @CreationTimestamp
    @Column(name = "tanggal")
    private Date tanggal;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users usersId;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "presensi")
    private PrsensiEnum presensi;

    public Long getId() {
        return id;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    public Date getMasuk() {
        return masuk;
    }

    public void setMasuk(Date masuk) {
        this.masuk = masuk;
    }


    public Date getTanggal() {
        return tanggal;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PrsensiEnum getPresensi() {
        return presensi;
    }

    public void setPresensi(PrsensiEnum presensi) {
        this.presensi = presensi;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }
}