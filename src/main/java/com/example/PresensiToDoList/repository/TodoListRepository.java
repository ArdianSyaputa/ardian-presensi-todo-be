package com.example.PresensiToDoList.repository;

import com.example.PresensiToDoList.modal.TodoList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoListRepository extends JpaRepository<TodoList, Long> {

    @Query(value = "SELECT * FROM todolist WHERE user_id = ?1", nativeQuery = true)
    List<TodoList> findAllUser(Long usersId);

}
