package com.example.PresensiToDoList.repository;

import com.example.PresensiToDoList.modal.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TemporaryTokenRepository extends JpaRepository<TemporaryToken,Integer> {
Optional<TemporaryToken> findByToken(String token);
Optional<TemporaryToken> findByUserId(Long userId);
}
