package com.example.PresensiToDoList.serviceImplements;


import com.example.PresensiToDoList.dto.PresensiDto;
import com.example.PresensiToDoList.enumated.PrsensiEnum;
import com.example.PresensiToDoList.exception.NotFoundException;
import com.example.PresensiToDoList.modal.Presensi;
import com.example.PresensiToDoList.repository.PresensiRepository;
import com.example.PresensiToDoList.repository.UsersRepository;
import com.example.PresensiToDoList.service.PresensiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PresensiServiceImpl implements PresensiService {
    private static final Integer hour = 3600 * 1000;
    @Autowired
    private PresensiRepository presensiRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public List<Presensi> getAllPresensi(Long usersId) {
        return presensiRepository.findAllUser(usersId);
    }

    @Override
    public Presensi addPresensiMasuk(PresensiDto presensiDto, boolean type) {
        Presensi presensi1 = new Presensi();
       presensi1.setMasuk(new Date(new Date().getTime() + 7 * hour));
        if (type == true) {
            presensi1.setPresensi(PrsensiEnum.MASUK);

        }else {
            presensi1.setPresensi(PrsensiEnum.PULANG);
        }
        presensi1.setUsersId(usersRepository.findById(presensiDto.getUsersId()).orElseThrow(() -> new NotFoundException("Not Found")));
        return presensiRepository.save(presensi1);
    }

    @Override
    public Presensi addPresensiPulang(PresensiDto presensiPulangDto, boolean type) {
        Presensi presensi1 = new Presensi();
        presensi1.setMasuk(new Date(new Date().getTime() + 7 * hour));
        presensi1.setUsersId(usersRepository.findById(presensiPulangDto.getUsersId()).orElseThrow(() -> new NotFoundException("Not Found")));
        return presensiRepository.save(presensi1);
    }

    @Override
    public Presensi getPresensi(Long id) {
        var Presensi = presensiRepository.findById(id).get();
        return presensiRepository.save(Presensi);
    }


    @Override
    public void deletePresensiById(Long id) {
presensiRepository.deleteById(id);
    }
}