package com.example.PresensiToDoList.serviceImplements;

import com.example.PresensiToDoList.dto.TodoListDto;
import com.example.PresensiToDoList.exception.NotFoundException;
import com.example.PresensiToDoList.modal.TodoList;
import com.example.PresensiToDoList.repository.TodoListRepository;
import com.example.PresensiToDoList.repository.UsersRepository;
import com.example.PresensiToDoList.service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoListServiceImpl implements TodoListService {

    @Autowired
    TodoListRepository todoListRepository;

    @Autowired
    UsersRepository usersRepository;

    @Override
    public List<TodoList> getAllTodoList(Long userId) {
        return todoListRepository.findAllUser(userId);
    }

    @Override
    public TodoList addtodoList(TodoListDto todoListDto) {
        TodoList toDoList1 = new TodoList();
        toDoList1.setTugas(todoListDto.getTugas());
        toDoList1.setKeterangan(todoListDto.getKeterangan());
        toDoList1.setUsersId(usersRepository.findById(todoListDto.getUserId()).orElseThrow(() -> new NotFoundException("Not Found")));
        return todoListRepository.save(toDoList1);

    }
    @Override
    public TodoList edittodoList(Long id, TodoListDto todoListDto) {
        TodoList todoList1 = todoListRepository.findById(id).get();
        todoList1.setTugas(todoListDto.getTugas());
        todoList1.setKeterangan(todoListDto.getKeterangan());
        todoList1.setUsersId(usersRepository.findById(todoListDto.getUserId()).orElseThrow(() -> new NotFoundException("error")));
        return todoListRepository.save(todoList1);
    }
    @Override
    public TodoList getTodoList(Long id) {
        return null;
    }

    @Override
    public void deleteTodoListById(Long id) {
todoListRepository.deleteById(id);
    }
}








