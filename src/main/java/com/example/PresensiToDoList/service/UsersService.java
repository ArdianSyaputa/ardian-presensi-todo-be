package com.example.PresensiToDoList.service;

import com.example.PresensiToDoList.dto.LoginDto;
import com.example.PresensiToDoList.modal.Users;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface UsersService {
    Map<String, Object> login(LoginDto loginDto);
    Object getAllUsers();
    Users addUsers(Users users,MultipartFile multipartFile );

    Users getUsers(Long id);
    Users editUsers(Long id,Users users, MultipartFile multipartFile);

    void deleteUsersById(Long id);
}
