package com.example.PresensiToDoList.service;

import com.example.PresensiToDoList.dto.TodoListDto;
import com.example.PresensiToDoList.modal.TodoList;

import java.util.List;

public interface TodoListService {
    List<TodoList> getAllTodoList(Long userId);

    TodoList addtodoList(TodoListDto todoListDto);

    TodoList edittodoList(Long id, TodoListDto todoListDto);

    TodoList getTodoList(Long id);

//    TodoList editTodoList(Integer id, Integer passagerTypeId, TodoList todoList);


    void deleteTodoListById(Long id);
}
