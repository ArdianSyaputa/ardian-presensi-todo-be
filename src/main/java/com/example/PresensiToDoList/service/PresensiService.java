package com.example.PresensiToDoList.service;

import com.example.PresensiToDoList.dto.PresensiDto;
import com.example.PresensiToDoList.modal.Presensi;


import java.util.List;

public interface PresensiService {

    List<Presensi> getAllPresensi(Long usersId);

    Presensi addPresensiMasuk(PresensiDto absenDto, boolean type);

    Presensi addPresensiPulang(PresensiDto presensiDto, boolean type);


//    Absen editAbsen(Long id, Long passagerTypeId, Absen absen);


    Object getPresensi(Long id);

    void deletePresensiById(Long id);
}
