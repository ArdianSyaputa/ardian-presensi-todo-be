package com.example.PresensiToDoList.controller;


import com.example.PresensiToDoList.dto.TodoListDto;
import com.example.PresensiToDoList.modal.TodoList;
import com.example.PresensiToDoList.response.CommonResponse;
import com.example.PresensiToDoList.response.ResponHelper;
import com.example.PresensiToDoList.service.TodoListService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

    @RestController
    @RequestMapping("/todolist")
    public class TodoListController {

        @Autowired
        private TodoListService todoListService;

        @Autowired
        private ModelMapper modalmepper;

        @GetMapping("/all")
        public CommonResponse<List<TodoList>> getAllTodoList(Long userId){
            return ResponHelper.ok(todoListService.getAllTodoList(userId)) ;
        }

        @GetMapping("/{id}")
        public CommonResponse getTodoList(@PathVariable("id") Long id) {
            return ResponHelper.ok(todoListService.getTodoList(id));
        }

        @PostMapping
        public CommonResponse<TodoList> addTodoList( @RequestBody TodoListDto todoListDto) {
            return ResponHelper.ok( todoListService.addtodoList(todoListDto)) ;
        }

        @PutMapping(path = "/{id}")
        public CommonResponse<TodoList> edittodoListById(@PathVariable("id") Long id,@RequestBody TodoListDto todoListDto) {
            return ResponHelper.ok( todoListService.edittodoList(id, todoListDto));
        }

        @DeleteMapping("/{id}")
        public void deleteTodoListById(@PathVariable("id") Long id) { todoListService.deleteTodoListById(id);}
    }
