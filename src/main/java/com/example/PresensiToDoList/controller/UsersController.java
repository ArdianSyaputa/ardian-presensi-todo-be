package com.example.PresensiToDoList.controller;

import com.example.PresensiToDoList.dto.LoginDto;
import com.example.PresensiToDoList.dto.ProfilDto;
import com.example.PresensiToDoList.dto.UserDto;

import com.example.PresensiToDoList.modal.Users;
import com.example.PresensiToDoList.response.CommonResponse;
import com.example.PresensiToDoList.response.ResponHelper;

import com.example.PresensiToDoList.service.UsersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.util.Map;

@RequestMapping("/user")
@RestController

public class UsersController {
    @Autowired
    private UsersService usersService;


    @Autowired
    private ModelMapper modelMapper;

@PostMapping("/sign-in")
public CommonResponse<Map<String,Object>>login(@RequestBody LoginDto loginDto){
    return ResponHelper.ok(usersService.login(loginDto));
}
    @PostMapping(value = "/sign-up", consumes = "multipart/form-data")
    public CommonResponse<Users> addUsers( UserDto userDto, @RequestPart("foto")MultipartFile multipartFile){
        return  ResponHelper.ok(usersService.addUsers(modelMapper.map(userDto, Users.class),multipartFile));
    }

    @GetMapping("/all")
    public  Object getAllUsers(){
        return ResponHelper.ok(usersService.getAllUsers());

    }
    @GetMapping("/{id}")
    public CommonResponse<Users> getUsers(@PathVariable("id")Long id){
        return ResponHelper.ok(usersService.getUsers(id));
    }

//        @PostMapping
//    public CommonResponse<Users> addUsers(@RequestBody Users users) {
//        return ResponseHelper.ok( usersService.addUsers(users)) ;
//    }
    @PutMapping(path = "/{id}", consumes = "multipart/form-data")
    public CommonResponse<Users> editUsersById(@PathVariable("id") Long id, ProfilDto profilDTO, @RequestPart("foto")MultipartFile multipartFile) {
        return ResponHelper.ok( usersService.editUsers( id,modelMapper.map(profilDTO, Users.class),multipartFile));
    }
    @DeleteMapping("/{id}")
    public void deleteUsersById(@PathVariable("id") Long id) { usersService.deleteUsersById(id);}
}
