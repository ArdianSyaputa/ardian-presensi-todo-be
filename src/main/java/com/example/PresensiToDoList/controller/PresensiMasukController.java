package com.example.PresensiToDoList.controller;


import com.example.PresensiToDoList.dto.PresensiDto;
import com.example.PresensiToDoList.modal.Presensi;
import com.example.PresensiToDoList.response.CommonResponse;
import com.example.PresensiToDoList.response.ResponHelper;
import com.example.PresensiToDoList.service.PresensiService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/AbsenMasuk")
@CrossOrigin(origins = "http://localhost:3000")
public class PresensiMasukController {

    @Autowired
    private PresensiService presensiService;

    @Autowired
    private ModelMapper modalmepper;

    @GetMapping("/all")
    public Object getAllPresensi(Long userId){
        return ResponHelper.ok(presensiService.getAllPresensi(userId)) ;
    }

    @GetMapping("/{id}")
    public CommonResponse getPresensi(@PathVariable("id") Long id) {
        return ResponHelper.ok(presensiService.getPresensi(id)) ;
    }
    @PostMapping("/ABSEN_MASUK")
    public CommonResponse<Presensi> addPresensiMasuk(PresensiDto presensiDto) {
        return ResponHelper.ok( presensiService.addPresensiMasuk(presensiDto,true));
    }

    @PostMapping("/ABSEN_PULANG")
    public CommonResponse<Presensi> addPresensiPulang (@RequestBody PresensiDto presensiDto) {
        return ResponHelper.ok( presensiService.addPresensiMasuk(presensiDto,false));
    }


    @DeleteMapping("/{id}")
    public void deletePresensiById(@PathVariable("id") Long id) { presensiService.deletePresensiById(id);}
}
